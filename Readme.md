Huron Docs
===========

## Requirements
Assuming you have Python already

Install Sphinx
```sh
$ pip install sphinx
$ cd docs
```

## Build doc
```sh
$ make clean
$ make html
```
